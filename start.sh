#!/bin/bash

set -eu -o pipefail

if [[ ! -f /app/data/hooks.json ]]; then
    echo "=> Generating config on first run"
    cat <<-EOF > /app/data/hooks.json
[
  {
    "__comment": "Example entry",
    "id": "webhook",
    "execute-command": "/app/code/ping.sh",
    "include-command-output-in-response": true,
    "command-working-directory": "/app/code"
  },
  {
    "__comment": "Lists configured hooks",
    "id": "available-hooks",
    "execute-command": "cat",
    "pass-arguments-to-command": [ { "source": "string", "name": "/app/data/hooks.json" } ],
    "include-command-output-in-response": true
  }
]
EOF

fi

echo "=> Start webhook"
cd /app/data/
exec /usr/local/bin/gosu cloudron:cloudron /app/code/webhook -verbose -hooks=/app/data/hooks.json -hotreload
