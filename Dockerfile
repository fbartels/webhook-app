FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

EXPOSE 9000

RUN mkdir -p /app/code /app/data
WORKDIR /app/data

RUN curl -L https://github.com/adnanh/webhook/releases/download/2.8.0/webhook-linux-amd64.tar.gz | tar -zxf - --strip-components=1 -C /app/code

COPY start.sh ping.sh /app/code/

CMD [ "/app/code/start.sh" ]
